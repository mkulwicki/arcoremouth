﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColorController : MonoBehaviour
{
    private MeshRenderer rend;
    [SerializeField]
    private Slider red;
    [SerializeField]
    private Slider green;
    [SerializeField]
    private Slider blue;
    [SerializeField]
    private Slider alpha;
    private static float holdLipTransparency = 1;
    private void Start() {
        rend = gameObject.GetComponent<MeshRenderer>();
    }
    private void Update() {
        rend.material.SetColor("_TintColor", new Color(red.value, green.value, blue.value, 1));
        rend.material.SetFloat("_Transparency", alpha.value * holdLipTransparency);
    }
    public static void holdTouch(){
        holdLipTransparency = .6f;
    }
    public static void endHoldTouch(){
        holdLipTransparency = 1f;
    }
}
