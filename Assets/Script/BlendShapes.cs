﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class BlendShapes : MonoBehaviour
{
    public Slider sliderBlendShape1;
    public Slider sliderBlendShape2;
    public Slider sliderBlendShape3;
    public Slider sliderMeshControl;
    List<int> _lips;
    SkinnedMeshRenderer skinnedMeshRenderer;
    private void Start() {
        skinnedMeshRenderer = GetComponent<SkinnedMeshRenderer>();

        _lips = DataMouthLocalization.MOUTH_UPPER.Concat(DataMouthLocalization.MOUTH_BOTTOM).ToList();
    }
    private void Update() {
        skinnedMeshRenderer.SetBlendShapeWeight(0, sliderBlendShape1.value);
        skinnedMeshRenderer.SetBlendShapeWeight(1, sliderBlendShape2.value);

        if(skinnedMeshRenderer != null){
           
            Debug.Log("Success");
        }
    }

    public List<Vector3> ChangeMeshPosition(List<Vector3> m_MeshVertices)
    {
        HashSet<int> mouthHashSet = new HashSet<int>(_lips);
        if (sliderMeshControl != null) m_MeshVertices = SetPosition(m_MeshVertices, mouthHashSet.ToList(), sliderMeshControl.value);
        return m_MeshVertices;
    }
    public List<int> GetMeshIndicies(){
        return _lips;
    }
    public List<Vector3> SetPosition(List<Vector3> m_MeshVertices, List<int> list, float changeValue)
    {
        for (int i = 0; i < list.Count; i++)
        {
            m_MeshVertices[list[i]] = new Vector3(
                m_MeshVertices[list[i]].x,
                m_MeshVertices[list[i]].y + changeValue,
                m_MeshVertices[list[i]].z);
        }
        return m_MeshVertices;
    }
    
}