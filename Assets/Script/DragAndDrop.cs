﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DragAndDrop : MonoBehaviour {
    public delegate void TouchHoldEnd();
    public static event TouchHoldEnd onTouchHoldEnd;
    private Vector3 startPointTouch;
    private Vector3 position;
    private Vector3 position2;
    private float distanceBetweenFingersInMultitouch = 0f;
    private float width;
    private float height;
    public bool multiTouch;
    public bool holdTouch = false;
    float touchStartTime = 0f;
    float lastChangeYPos = 0f;
    float lastChangeXPos = 0f;
    public float GetYPositionChange(){
        return lastChangeYPos = startPointTouch.y - position.y;
    }
    public float GetXPositionChange(){ 
        return lastChangeXPos = startPointTouch.x - position.x;
    }
    public float GetStartPosY(){
        return startPointTouch.y;
    }
    public float GetStartPosX(){
        return startPointTouch.x;
    }
    public float Distance(){
        return Vector3.Distance(position2, position) - distanceBetweenFingersInMultitouch;
    }

    void Awake()
    {
        width = (float)Screen.width / 2.0f;
        height = (float)Screen.height / 2.0f;

        // Position used for the cube.
        startPointTouch = new Vector3(0f, 0f , 0f);
        position = new Vector3(0.0f, 0.0f, 0.0f);
        position2 = new Vector3(0.0f, 0.0f, 0.0f);
        startPointTouch = new Vector3(0.0f, 0.0f, 0f);
        multiTouch = false;
    }

    // void OnGUI()
    // {
    //     // Compute a fontSize based on the size of the screen width.
    //     GUI.skin.label.fontSize = (int)(Screen.width / 25.0f);

    //     GUI.Label(new Rect(20, 20, width, height * 0.25f),
    //         "x = " + position2.x.ToString("f2") +
    //         ", y = " + position2.y.ToString("f2") + Distance());
    // }

    void Update()
    {
        // Handle screen touches.
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);

            if(!multiTouch && !EventSystem.current.IsPointerOverGameObject(touch.fingerId)){
                if(touch.phase == TouchPhase.Began){
                    startPointTouch = SetVectorPos(touch.position);
                    touchStartTime = Time.time;
                }
                if (touch.phase == TouchPhase.Moved && (Time.time - touchStartTime > 1f))
                {
                    holdTouch = true;
                    ColorController.holdTouch();
                    position = SetVectorPos(touch.position);
                }
                if(touch.phase == TouchPhase.Ended){
                    ColorController.endHoldTouch();
                    holdTouch = false;
                    if(onTouchHoldEnd != null) onTouchHoldEnd();
                }
            }
            if (Input.touchCount == 2 && !holdTouch)
            {
                position = SetVectorPos(touch.position);
                multiTouch = true;
                touch = Input.GetTouch(1);
                if(touch.phase == TouchPhase.Began){
                    position2 = SetVectorPos(touch.position);
                    distanceBetweenFingersInMultitouch = Vector3.Distance(position2, position);
                }
                if (touch.phase == TouchPhase.Moved)
                {
                    position2 = SetVectorPos(touch.position);
                }
                if (touch.phase == TouchPhase.Ended)
                {
                    multiTouch = false;
                    if(onTouchHoldEnd != null) onTouchHoldEnd();
                    distanceBetweenFingersInMultitouch = 0f;
                }
            }
        }
    }
    Vector3 SetVectorPos(Vector2 touchPossition){

        Vector2 pos2 = touchPossition;
        pos2.x = (pos2.x - width) / width;
        pos2.y = (pos2.y - height) / height;
        return new Vector3(-pos2.x, pos2.y, 0.0f);
    }
}