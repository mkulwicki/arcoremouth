﻿using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using UnityEngine.UI;

public class BlendShapesJsonReader : MonoBehaviour
{
    public List<string> filenames;
    private Dictionary<string, BlendShapesJson> data;
    private void Start() {
        if(filenames != null){
            foreach(var name in filenames){
                GenerateDataFromFiles(name);
            }
        }
    }
    private void Init(){
        if(data == null){
             data = new Dictionary<string, BlendShapesJson>();
        }
    }
    private void GenerateDataFromFiles(string filename){
        var jsonBlend = Resources.Load<TextAsset>("BlendShapes/"+filename);

        if(jsonBlend != null){
            Init();

            BlendShapesJson item = JsonConvert.DeserializeObject<BlendShapesJson>(jsonBlend.ToString());

            foreach(var morph in item.morphTargets){
                morph.GenerateVerticles(item.valuesAllPointsBase);
            }

            data.Add(filename,item);
            Debug.Log("Add file to data:  "+filename+" : "+item.ToString());
            
        }else{
            Debug.Log("FILE NOT FOUND:  "+filename);
        }
    }
    public List<MorphTargets> GetBlendShapesByNameFromFile(string name){

        BlendShapesJson blend = new BlendShapesJson();
        if(data.TryGetValue(name, out blend)){
            return blend.morphTargets;
        }
        return null;
    }
}