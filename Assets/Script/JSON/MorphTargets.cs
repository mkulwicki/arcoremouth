﻿using System.Collections.Generic;
using UnityEngine;
using System;
using Newtonsoft.Json;

[System.Serializable]
public class MorphTargets{
        [JsonProperty("name")]
        public string name { get; set; }
        [JsonProperty("vertices")]
        public float[] valuesAllPoints { get; set; }
        public List<Vector3> verticles;
        public void GenerateVerticles(float[] valuesAllPointsBase){
            this.verticles = new List<Vector3>();

            CalculateChangeAllPointsValues(valuesAllPointsBase);

            for(int i=0; i < this.valuesAllPoints.Length ; i+=3){
                this.verticles.Add(new Vector3(this.valuesAllPoints[i], this.valuesAllPoints[i+1], this.valuesAllPoints[i+2]));
            }
        }
        void CalculateChangeAllPointsValues(float[] valuesAllPointsBase){
            for(int i=0; i < this.valuesAllPoints.Length ; i++){

                this.valuesAllPoints[i] = (float) Math.Round( (decimal) (this.valuesAllPoints[i] - valuesAllPointsBase[i]), 6);
                // this.valuesAllPoints[i] = (float) Math.Round( (decimal) (Mathf.Abs(valuesAllPointsBase[i]) - Mathf.Abs(this.valuesAllPoints[i]) ), 6);
            }
        }
    }
