﻿using System.Collections.Generic;
using Newtonsoft.Json;

[System.Serializable]
public class BlendShapesJson{
        [JsonProperty("vertices")]
        public float[] valuesAllPointsBase { get; set; }
        [JsonProperty("morphTargets")]
        public List<MorphTargets> morphTargets { get; set; }
    }
