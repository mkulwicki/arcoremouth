﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class MouthController : MonoBehaviour
{
    public DragAndDrop drag;
    List<int> _lips;
    enum Lip { UPPER_LIP , BOTTOM_LIP };
    Dictionary<string,Lip> enumDictionary = new Dictionary<string, Lip>();
    public BlendShapesJsonReader blendShapesJsonReader;
    private void Start() {
        enumDictionary.Add("UPPER_LIP", Lip.UPPER_LIP);
        enumDictionary.Add("BOTTOM_LIP", Lip.BOTTOM_LIP);

        _lips = DataMouthLocalization.MOUTH_UPPER.Concat(DataMouthLocalization.MOUTH_BOTTOM).ToList();
    }
    private void OnEnable() {
        DragAndDrop.onTouchHoldEnd += SaveLipPos;
    }
    private void OnDisable() {
        DragAndDrop.onTouchHoldEnd -= SaveLipPos;
    }
    // public List<Vector3> ChangeMeshPosition(List<Vector3> m_MeshVertices)
    // {
    //     HashSet<int> mouthHashSet = new HashSet<int>(DataMouthLocalization.MOUTH_UPPER.Concat(DataMouthLocalization.MOUTH_BOTTOM));
    //     if (sliderMeshControl != null) m_MeshVertices = SetPosition(m_MeshVertices, mouthHashSet.ToList(), sliderMeshControl.value);
    //     return m_MeshVertices;
    // }
    public List<int> GetMeshIndicies(string name){

        Lip lip;
        if(enumDictionary.TryGetValue(name, out lip)){
            switch(lip){
                case Lip.UPPER_LIP:
                {
                    return DataMouthLocalization.MOUTH_UPPER;
                }
                case Lip.BOTTOM_LIP:
                {
                    return DataMouthLocalization.MOUTH_BOTTOM;
                }
            }
        }else{
            Debug.LogWarning(this.GetType().Name+ "ERROR Name of GameObject is not the same as name in Dictiorany");
        }
        return _lips;
    }

    public List<Vector3> SetPosition(List<Vector3> vtx, List<int> list, float changeValue)
    {
        for (int i = 0; i < list.Count; i++)
        {
            vtx[list[i]] = new Vector3(
                vtx[list[i]].x,
                vtx[list[i]].y + changeValue,
                vtx[list[i]].z);
        }

        return vtx;
    }
    public List<Vector3> ChangeMeshPossitionByBlendShapes(List<Vector3> m_MeshVertices){

        List<MorphTargets> morphTargets = blendShapesJsonReader.GetBlendShapesByNameFromFile(blendShapesJsonReader.filenames[0]);
        if(morphTargets != null){
            
            Gesture(m_MeshVertices, morphTargets);
        }else{
            Debug.Log(this.GetType().Name+" Morph Target is NULL for filename: "+name);
        }
        
        return m_MeshVertices;
    }
    public List<Vector3> SetKiss(List<Vector3> m_MeshVertices, List<int> upperLip , List<int> lowerLip){
        
        float distance = Vector3.Distance(m_MeshVertices[306] , m_MeshVertices[76]);
        float distanceUpperLower = Vector3.Distance(m_MeshVertices[upperLip[0]] , m_MeshVertices[lowerLip[0]]);
        if( (distance < 0.047f && distanceUpperLower < 0.0125f) || distanceUpperLower < 0.0048f){
            for (int i = 0; i < upperLip.Count; i++)
            {
                    m_MeshVertices[upperLip[i]] = new Vector3(
                    m_MeshVertices[lowerLip[i]].x,
                    m_MeshVertices[lowerLip[i]].y,
                    m_MeshVertices[lowerLip[i]].z);
            }
        }
        return m_MeshVertices;
    }

    private float lipCorenersLastPosXY = 0f;
    private bool isLipCorenersLastPosXY = false;
    private float lipVerticalLastPosXY = 0f;
    private bool isLipVerticalLastPosXY = false;
    private float lipWidthLastPosXY = 0f;
    private bool isLipWidthChange = false;
    private float sizeUpperLip = 0f;
    private bool isSizeUpperLipChange = false;
    private float sizeLowerLip = 0f;
    private bool isSizeLowerLipChange = false;
    private List<Vector3> Gesture(List<Vector3> m_MeshVertices, List<MorphTargets> morphTargets){
        
        //Lip corners
        if(IsInRange(drag, 0.8f, 1f, -0.5f , 0.5f) && drag.holdTouch || IsInRange(drag, -1f, -0.8f, -0.5f , 0.5f) && drag.holdTouch){
            m_MeshVertices = SetMorphById(0,lipCorenersLastPosXY + drag.GetYPositionChange()/200 , m_MeshVertices, morphTargets);
            isLipCorenersLastPosXY = true;
        }else m_MeshVertices = SetMorphById(0, lipCorenersLastPosXY , m_MeshVertices, morphTargets);
        //Lip pos Up/Down
        if(IsInRange(drag, -.7f, .7f, -0.5f , 0.5f) && drag.holdTouch){
            m_MeshVertices = SetMorphById(1,lipVerticalLastPosXY + drag.GetYPositionChange()/200 , m_MeshVertices, morphTargets);
            isLipVerticalLastPosXY = true;
        }else m_MeshVertices = SetMorphById(1, lipVerticalLastPosXY , m_MeshVertices, morphTargets);
        //Lip width
        if(IsInRange(drag, -.7f, .7f, -0.5f , 0.5f) && drag.holdTouch){
            m_MeshVertices = SetMorphById(2,lipWidthLastPosXY + drag.GetXPositionChange()/100 , m_MeshVertices, morphTargets);
            isLipWidthChange = true;
        } 
        else m_MeshVertices = SetMorphById(2, lipWidthLastPosXY , m_MeshVertices, morphTargets);
        //UpperLip size
        if(IsInRange(drag, -1f, 1f, 0f , 1f) && !isSizeLowerLipChange && drag.multiTouch){
            m_MeshVertices = SetMorphById(3,sizeUpperLip + drag.Distance()/100 , m_MeshVertices, morphTargets);
            isSizeUpperLipChange = true;
        }else m_MeshVertices = SetMorphById(3, sizeUpperLip , m_MeshVertices, morphTargets);
        // //LowerLip size
        if(IsInRange(drag, -1f, 1f, -1f , 0f) && !isSizeUpperLipChange && drag.multiTouch){
            m_MeshVertices = SetMorphById(4,sizeLowerLip + drag.Distance()/100 , m_MeshVertices, morphTargets);
            isSizeLowerLipChange = true;
        }else m_MeshVertices = SetMorphById(4, sizeLowerLip , m_MeshVertices, morphTargets);


        return m_MeshVertices;
    }
    void SaveLipPos(){
        
        if(isLipWidthChange) lipWidthLastPosXY += drag.GetXPositionChange()/100;
        if(isLipVerticalLastPosXY) lipVerticalLastPosXY += drag.GetYPositionChange()/200;
        if(isLipCorenersLastPosXY) lipCorenersLastPosXY += drag.GetYPositionChange()/200;
        if(isSizeUpperLipChange) sizeUpperLip += drag.Distance()/100;
        if(isSizeLowerLipChange) sizeLowerLip += drag.Distance()/100;

        lipWidthLastPosXY = CheckMinAndMaxValue(-.025f, .012f, lipWidthLastPosXY);
        lipVerticalLastPosXY = CheckMinAndMaxValue(-.009f, .009f, lipVerticalLastPosXY);
        lipCorenersLastPosXY = CheckMinAndMaxValue(-.015f, .015f, lipCorenersLastPosXY);
        sizeUpperLip = CheckMinAndMaxValue(-.0065f, .025f, sizeUpperLip);
        sizeLowerLip = CheckMinAndMaxValue(-.012f, .015f, sizeLowerLip);
        
        isLipWidthChange = false;
        isLipVerticalLastPosXY = false;
        isLipCorenersLastPosXY = false;
        isSizeUpperLipChange = false;
        isSizeLowerLipChange = false;
    }

    float CheckMinAndMaxValue(float min, float max, float value){
        if(value < min) return min;
        if(value > max) return max;
        return value;
    }

    private bool IsInRange(DragAndDrop drag,float minX, float maxX, float minY, float maxY){

        if(drag.GetStartPosX() >= minX && drag.GetStartPosX() <= maxX && drag.GetStartPosY() >= minY && drag.GetStartPosY() <= maxY) return true;
        return false;
    }
    private List<Vector3> SetMorphById(int id , float dragPosValue , List<Vector3> m_MeshVertices, List<MorphTargets> morphTargets){
        int limitt = m_MeshVertices.Count() < morphTargets[id].verticles.Count() ? m_MeshVertices.Count() : morphTargets[1].verticles.Count();
        for(int iMeshVec = 0 ; iMeshVec < limitt; iMeshVec++){
            m_MeshVertices[iMeshVec] = new Vector3(
                m_MeshVertices[iMeshVec].x + (morphTargets[id].verticles[iMeshVec].x * dragPosValue ) ,
                m_MeshVertices[iMeshVec].y + (morphTargets[id].verticles[iMeshVec].y * dragPosValue ) , 
                m_MeshVertices[iMeshVec].z + (morphTargets[id].verticles[iMeshVec].z * dragPosValue )
            );
        }
        return m_MeshVertices;
    }
}