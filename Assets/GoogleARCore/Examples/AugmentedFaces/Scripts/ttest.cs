﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ttest : MonoBehaviour
{

    SkinnedMeshRenderer skinnedMeshRenderer1;
    SkinnedMeshRenderer skinnedMeshRenderer2;
    SkinnedMeshRenderer skinnedMeshRenderer3;
    // Start is called before the first frame update
    void Start()
    {
        skinnedMeshRenderer1 = GetComponent<SkinnedMeshRenderer>();
        skinnedMeshRenderer2 = GetComponentInChildren<SkinnedMeshRenderer>();
        skinnedMeshRenderer3 = GetComponentInParent<SkinnedMeshRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if(skinnedMeshRenderer1 != null){
            Debug.Log(this.name+"skinnedMeshRenderer1");
        }
        if(skinnedMeshRenderer2 != null){
            Debug.Log(this.name+"skinnedMeshRenderer2");
        }
        if(skinnedMeshRenderer3 != null){
            Debug.Log(this.name+"skinnedMeshRenderer3");
        }
    }
}
